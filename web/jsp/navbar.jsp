<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="Articles">
            <img src="image/i++.png" alt="" height="30px">
        </a>
        <ul class="nav navbar-nav">
            <li class="active"><a href="Articles">Home</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <%String username = (String) session.getAttribute("username");%>
            <c:choose>
                <c:when test="${username != null}">
                    <li>
                        <button type="button" class="btn createArticleBtn" data-toggle="modal" data-target="#newArticleModal">
                            Create New Article
                        </button>
                    </li>
                    <li><a href="#"><img class="tinyimg" style="width: 30px; height: 30px" src="image/${image}"> Welcome : ${username} </a> </li>

                    <li ><a  class="navbarBtn" style="padding-top:20px; padding-bottom: 20px" href="HomePage?status=viewMyBlog">My
                        blog</a></li>
                    <li><a style="padding-top:20px; padding-bottom: 20px" href="UpdateInfoServlet"> My
                        Account</a></li>
                    <li><a style="padding-top:20px; padding-bottom: 20px" href="LoginServlet?status=logout"> Logout</a></li>
                </c:when>
                <c:otherwise>
                    <li><a style="padding-top:20px; padding-bottom: 20px" href="login.html"> Login</a></li>
                    <li><a style="padding-top:20px; padding-bottom: 20px" href="registration.html"> Register</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</nav>

<%--when users click the "create" button, direct them to this modal--%>
<%@include file="newArticleModal.jsp" %>
