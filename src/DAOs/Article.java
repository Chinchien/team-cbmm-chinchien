package DAOs;

import java.util.Date;

public class Article {
    private int articleId;
    private String title;
    private String content;
    private Date modifiedDateAndTime;
//    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-/MM-dd hh:mm:ss");
    private String status;
    private String username;

    public Date getModifiedDateAndTime() {
        return modifiedDateAndTime;
    }

    public void setModifiedDateAndTime(Date modifiedDateAndTime) {
        this.modifiedDateAndTime = modifiedDateAndTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    }



