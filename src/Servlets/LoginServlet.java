/**
 * created by: chinchien
 * - login checking
 **/
package Servlets;

import DAOs.User;
import DAOs.UserDAO;
import Recaptcha.VerifyRecaptcha;
import org.owasp.encoder.Encode;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {
    //if we get any error msg, set session's attribute "loginError" to the message that we want to show up
    //when go back to the login page
    private String loginError;
    Cookie cookie = new Cookie("loginError", ""); //initialise the cookie "loginError"

    public static final byte[] SALT= {45, 57, 11, 88};
    public static final int ITERATIONS=10;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.addIntHeader("X-XSS-Protection", 1);
        response.addHeader("X-Content-Type-Options", " nosniff");
        response.addHeader("X-Frame-Options", "deny");

        //users will be direct to the main page (ArtcileServlet) whatever they clicked the login or logout button
        //logout

        String status = Encode.forJava(request.getParameter("status")) ;
        //logout
        if (status != null && status.equals("logout")) {
            request.getSession().setAttribute("username", null);
            response.sendRedirect("Articles");
        }
        //login
        else {
            String username =  Encode.forJava(request.getParameter("username")) ;
            String inputPassword = Encode.forJava(request.getParameter("password")) ;
            String password = null;
            try (UserDAO userdao = new UserDAO()) {
                //check PW
                password = userdao.getPassword(username);
                if (password == null) { //if the user does not exit
                    // direct back to the login page, and store an error message in cookie
                    //PS: a cookie can only have 32 chars
                    loginError = "1"; //"incorrect username"
                    cookie = new Cookie("loginError", loginError);
                    response.addCookie(cookie);
                    response.sendRedirect("/login.html");
                }
                //check if the password exits: the input pw need to match the hashed pw
                if (password != null) {
                    //recaptcha
                    String gRecaptchaResponse = request
                            .getParameter("g-recaptcha-response");
                    boolean isSame = VerifyRecaptcha.verify(gRecaptchaResponse);

                   char[] input= inputPassword.toCharArray();
                   byte[] expectedPassword= Passwords.base64Decode(password);
                   boolean isMatched=Passwords.isExpectedPassword(input,SALT,ITERATIONS,expectedPassword);
                   if (!isMatched) {
                        loginError = "2"; //"incorrect password"
                        cookie = new Cookie("loginError", loginError);
                        response.addCookie(cookie);
                        response.sendRedirect("/login.html");
                    }
                    //Massie : verify that recaptcha is done correctly
                   else if (!isSame){
                        loginError = "3"; //"incorrect recaptcha"
                        cookie = new Cookie("loginError", loginError);
                        response.addCookie(cookie);
                        response.sendRedirect("/login.html");
                    }

                    else {
                        User user=userdao.getAllUserInfoByName(username);
                        String image=user.getAvatar();
                        request.getSession().setAttribute("image",image);
                        request.getSession().setAttribute("username", username);
                        request.getSession().setMaxInactiveInterval(480);
                        //direct to the main page
                        response.sendRedirect("Articles");
                    }
                }
            } catch (SQLException e) {
                loginError = "4"; //"Our system is not available now"
                cookie = new Cookie("loginError", loginError);
                response.addCookie(cookie);
                response.sendRedirect("/login.html");
            } catch (Exception e) {
                loginError = "4"; //"Our system is not available now"
                cookie = new Cookie("loginError", loginError);
                response.addCookie(cookie);
                response.sendRedirect("/login.html");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
