package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.CommentDAO;
import SecurityPack.AuthenticationModule;
import com.google.gson.Gson;
import org.owasp.encoder.Encode;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class CommentServlet extends HttpServlet {

    org.owasp.html.PolicyFactory supplement = new org.owasp.html.HtmlPolicyBuilder().allowUrlProtocols("data").toFactory();
    PolicyFactory policy = Sanitizers.BLOCKS.and(Sanitizers.IMAGES).and(Sanitizers.STYLES).and(Sanitizers.BLOCKS).and(Sanitizers.LINKS).and(supplement);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");

        doPost(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");

        Cookie[] cookies = req.getCookies();

        //loop through cookies to determine which business is at hand

        for (int i = 0; i < cookies.length; i++) {
            String name = cookies[i].getName();
            String value = cookies[i].getValue();


            if (value.equals("delete")) {

                req.getSession().setAttribute("commentID", Encode.forHtml(req.getParameter("commentID")));

                deleteComment(req, resp);
                //delete cookie to prevent confusion of future business
                cookies[i].setMaxAge(0);

            }
            if (value.equals("create")) {


                req.getSession().setAttribute("commentText", Encode.forJava(req.getParameter("text")));
                req.getSession().setAttribute("articleID", Encode.forJava(req.getParameter("articleID")));

                //if comment is a nested comment, set an additional parameter in the session
                if (req.getParameterMap().containsKey("rID")) {

                    req.getSession().setAttribute("rID", Encode.forJava(req.getParameter("rID")));
                }


                makeNewComment(req, resp);
                cookies[i].setMaxAge(0);

            }
        }


    }


    private void makeNewComment(HttpServletRequest req, HttpServletResponse resp) {

        HttpSession session = req.getSession();
        try (CommentDAO makeNewComment = new CommentDAO()) {
            Comment comment = new Comment();

            String newContent = policy.sanitize(Encode.forHtml((String) session.getAttribute("commentText")) );

            String placeHolder = (String) session.getAttribute("articleID");
            String image = (String) session.getAttribute("image");

            String refID = (String) session.getAttribute("rID");


            if (refID == null) {
        //create a regular comment
                int articleID = Integer.parseInt(placeHolder);


                String userName = "Guest";

                if (!(session.getAttribute("username")==null)){
                   userName= Encode.forJava((String) session.getAttribute("username"));
                }




                comment.setArticleId(articleID);
                comment.setContent(newContent);
                comment.setUsername(userName);
                comment.setAvatar(image);

                makeNewComment.createNewComment(comment);


            } else {

                //create a nested comment because it has a reference to another comment inside it
                int referenceID = Integer.parseInt((String) session.getAttribute("rID"));


                int articleID = Integer.parseInt(placeHolder);
                String userName = ((String) session.getAttribute("username"));
                if ((userName == null) || (userName.equals("")) || (userName.isEmpty())) {
                    userName = "guest";
                }

                comment.setReferenceId(referenceID);
                comment.setArticleId(articleID);
                comment.setContent(newContent);
                comment.setUsername(userName);
                comment.setAvatar(image);

                makeNewComment.createNestedComment(comment);


            }


        } catch (Exception e) {
            System.out.println("Exception occured here");

        }
    }


    private void deleteComment(HttpServletRequest request, HttpServletResponse response) {

        //this method creates an ArticleDAO and from there, calls the delete article method to remove it from the database

        String commentID = (String) request.getSession().getAttribute("commentID");
        int id = Integer.parseInt(commentID);
        try (CommentDAO toControl = new CommentDAO()) {

            Comment toCheck = toControl.getCommentById(id);

            AuthenticationModule toAuthenticate = new AuthenticationModule();
            boolean isSafeToDelete = toAuthenticate.check(request.getSession(), toCheck);

            if (isSafeToDelete) {

                toControl.deleteComment(id);
                request.getRequestDispatcher("articles.jsp").forward(request, response);

            } else {

                request.getRequestDispatcher("/jsp/ERROR/noPermission.jsp").forward(request, response);

            }


        } catch (Exception e) {
            System.out.println("Exception occured here");

        }


    }
}
