/**
 * created by:
 * modified & corrected by: Chinchien
 * - register: for updating & displaying
 **/
package Servlets;

import DAOs.User;
import DAOs.UserDAO;
import SecurityPack.AuthenticationModule;
import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;
import org.owasp.encoder.Encode;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "UpdateInfoServlet")
public class UpdateInfoServlet extends HttpServlet {
    private SmartUpload su;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.addIntHeader("X-XSS-Protection", 1);
        response.addHeader("X-Content-Type-Options", " nosniff");
        response.addHeader("X-Frame-Options", "deny");

        //for updating
        //chinchien: initialise smartUpload object
        su = new SmartUpload();
        su.initialize(this, request, response);
        su.setMaxFileSize(1024 * 1024 * 10);
        su.setAllowedFilesList("jpg,jpeg,bmp,png");
        try {
            su.setDeniedFilesList("exe,jsp,bat,html,,");
            su.upload();
        } catch (SQLException e) {
            System.out.println(" An Exception occured");
        } catch (SmartUploadException e) {
            System.out.println(" An Exception occured");
        }

        User userInfo;
        try (UserDAO userdao = new UserDAO()) {
            User updateUser = userdao.getAllUserInfoByName((su.getRequest().getParameter("username")));

            AuthenticationModule toCheck=new AuthenticationModule();
            HttpSession toAuthenticate=request.getSession();
            boolean safeToModify=toCheck.check(toAuthenticate,updateUser);
            if (safeToModify){
                //chinchien: set values by using SmartUpload object
                updateUser.setUsername((su.getRequest().getParameter("username")));
                updateUser.setFname((su.getRequest().getParameter("fname")) );
                updateUser.setLname((su.getRequest().getParameter("lname")));
                updateUser.setEmail((su.getRequest().getParameter("email")));
                updateUser.setDob((su.getRequest().getParameter("dob")));
                updateUser.setCountry((su.getRequest().getParameter("country")));
                updateUser.setDescrp((su.getRequest().getParameter("descrp")));
            }else {
                request.getRequestDispatcher("/jsp/noPermission.jsp").forward(request, response);
            }
            //chinchien: modify img
            if (!su.getRequest().getParameter("avatar").equals("myImg")) {
                updateUser.setAvatar(su.getRequest().getParameter("avatar"));
            } else {
                try {
                    String ext = su.getFiles().getFile(0).getFileExt(); //file type
                    String filename = su.getRequest().getParameter("username") + "." + ext;
                    if (filename != null){
                        String pa = this.getServletContext().getRealPath("/") + "image" + File.separator + filename;
                        su.getFiles().getFile(0).saveAs(pa);
                        File folder = new File(this.getServletContext().getRealPath("/") + "image");
                        File file = new File(pa);
                        generateThumbnail(folder, file);

                        updateUser.setAvatar(filename); }
                    else {
                        updateUser.setAvatar("avatar_default.png");
                    }
                } catch (SmartUploadException e) {
                    System.out.println(" An Exception occured");
                }
            }

            userdao.modifyUser(updateUser);

            userInfo = userdao.getAllUserInfoByName((su.getRequest().getParameter("username")));
            request.setAttribute("userInfo", userInfo);
            request.setAttribute("status", "done");
            request.getRequestDispatcher("/jsp/UpdateInfo.jsp").forward(request, response);
        } catch (SQLException e) {
            response.sendRedirect("/jsp/badUpdate.jsp");


        } catch (Exception e) {
            response.sendRedirect("/jsp/badUpdate.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.addIntHeader("X-XSS-Protection", 1);
        response.addHeader("X-Content-Type-Options", " nosniff");
        response.addHeader("X-Frame-Options", "deny");


        //for displaying
        String username = (String) request.getSession().getAttribute("username");
        String trustedUserName=Encode.forJava(username);
        //get userInfo from database
        User userInfo;
        try (UserDAO userdao = new UserDAO()) {
            userInfo = userdao.getAllUserInfoByName(trustedUserName);
            request.setAttribute("userInfo", userInfo);
            request.getRequestDispatcher("/jsp/UpdateInfo.jsp").forward(request, response);
        } catch (SQLException e) {
            response.sendRedirect("/login.html");
        } catch (Exception e) {
            response.sendRedirect("/login.html");
        }
    }

    private File generateThumbnail(File folder, File originalImageFile) throws IOException {

        final int MAX_W = 400, MAX_H = 400;

        Image thumbnail = null;
        BufferedImage original = null;

        // read original image
        original = ImageIO.read(originalImageFile);

        // get its dimensions
        int w = original.getWidth(null);
        int h = original.getHeight(null);

        // check if we can leave the original dimensions intact
        if (w <= MAX_W & h <= MAX_H) {

            // no resizing
            thumbnail = original;

        } else {

            // one or both dimensions exceed the max allowed size
            float thumbWidth, thumbHeight;
            float scaleFactor = 1.0f;

            // Figure out the scaleFactor - the number by which we should multiply the thumbmail's width and height.
            if (w > h) {
                thumbWidth = MAX_W;
                scaleFactor = thumbWidth / (float) w;
                thumbHeight = (float) h * scaleFactor;
            } else {
                thumbHeight = MAX_H;
                scaleFactor = thumbHeight / (float) h;
                thumbWidth = (float) w * scaleFactor;
            }

            // Actually create the scaled image
            thumbnail = original.getScaledInstance(Math.round(thumbWidth), Math.round(thumbHeight), Image.SCALE_SMOOTH);

        }

        // Write out the thumbnail
        BufferedImage buffer = new BufferedImage(thumbnail.getWidth(null), thumbnail.getHeight(null), original.getType());
        buffer.createGraphics().drawImage(thumbnail, 0, 0, null);
        File thumbnailFile = new File(folder, "thumbnail.png");
        ImageIO.write(buffer, "png", thumbnailFile);
        return thumbnailFile;
    }
}
