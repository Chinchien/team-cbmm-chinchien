<%--This modal displays the articles within our web application--%>

<div class="modal fade" id="articleModal" tabindex="-1" role="dialog" aria-labelledby="modal_title"
     aria-hidden="true" style="width: 96%; height: 100%; margin-left: 2%; margin-right: 2%">
    <div class="modal-dialog" style="width: 100%" role="document">
        <div class="modal-content row" style="width: 100%">
            <div class="modal-body col-md-8">

                <%--The following four tags update according to the content of the article--%>
                <h3 class="modal_title"></h3>
                <h5 class="modal_user"></h5>
                <p id="modal_id"></p>
                <p class="modal_txt"></p>
            </div>
            <div class="col-md-3">
                <div id="commentArea" style="background-color: #e0e3e4">

                    <%--This JSTL loop prints the comments when there are comments in the database--%>
                    <c:choose>
                        <c:when test="${fn:length(comments) gt 0}">
                            <c:forEach items="${comments}" var="comment">

                                <div class="commentArticleId">${comment.articleId}</div>

                                <div id="${comment.commentId}">
                                    <div class="commentz" data-id="${comment.articleId}"><img
                                            src="image/${comment.avatar}" class="tinyImg"> ${comment.content}
                                        <button data-toggle="modal" data-target="#nestedModalz"
                                                onclick="idToNested(${comment.commentId})" class="smallClick">
                                            <img class="cmtImgz" src="image/comment.png">

                                        </button>
                                        <c:choose>
                                            <c:when test="${comment.username==username}">
                                                <button type="button" class="deleteButtonz" id="deleteButton"
                                                        onclick="deleteComment(${comment.commentId})">
                                                    <img class="dltCmtImgz" src="image/delete.png">
                                                </button>
                                            </c:when>
                                        </c:choose>


                                        <c:set var="timeString" value="${comment.createdDate}"/>
                                        <div><p class="commentStamp"> left by: ${comment.username}
                                            at ${fn:substring(timeString, 5, 16)} </p></div>
                                    </div>
                                    <div class="nestedComment"></div>

                                </div>
                                <div class="commentArticleId">${comment.articleId}</div>


                                <%--This JSTL loop prints all the nested comments--%>
                                <c:choose>
                                    <c:when test="${fn:length(nestedComments) gt 0}">
                                        <c:forEach var="ncomment" items="${nestedComments}">
                                            <c:choose>
                                                <c:when test="${ncomment.referenceId==comment.commentId}">

                                                    <div class="tinyCommentsz"><img src="../image/${ncomment.avatar}"
                                                                                    class="tinyImg"> ${ncomment.content}
                                                        <c:set var="timeString" value="${ncomment.createdDate}"/>
                                                        <div><p class="commentStamp"> left by: ${ncomment.username}
                                                            at ${fn:substring(timeString, 5, 16)} </p></div>
                                                    </div>
                                                    <div class="tinyID" hidden> ${ncomment.articleId}</div>
                                                </c:when>
                                                <c:otherwise> </c:otherwise>
                                            </c:choose>

                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise><p></p></c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:when>
                        <c:otherwise><p> No comments here!</p></c:otherwise>
                    </c:choose>
                </div>

                <div>
                    <%--This is the form for commenting and editing the article--%>
                    <form method="post" action="Articles" id="articleForm">

                        <textarea rows="2" cols="50" id="commentTextArea"></textarea>
                        <button type="button" class="btn btn-primary" data-toggle="modal" onclick="leaveComment()"
                                id="commentButton">Comment
                        </button>
                        <%--no value of id parse to servlet "article"--%>
                        <input type="text" id="editID" name="editID">
                        <%--<input type="text" id="articleID" name="articleID" readonly>--%>
                        <input type="text" id="editTitle" name="editTitle" required>
                        <span style="color: red">*</span>
                        <textarea id="editTextArea" name="editTextArea"></textarea>
                        <button type="submit" class="btn btn-primary save" data-toggle="modal" onclick="editCookie()"
                                id="saveButton">Save
                        </button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>

                <button id="editButton" type="button" class="btn btn-primary edit" data-toggle="modal"
                        onclick="displayEdit()">Edit
                    Article
                </button>
            </div>
        </div>
    </div>
</div>

<%--chinchien: WYSIWYG--%>
<script>
    $('.summernote').summernote({
        height: 300,
        focus: true
    });
</script>