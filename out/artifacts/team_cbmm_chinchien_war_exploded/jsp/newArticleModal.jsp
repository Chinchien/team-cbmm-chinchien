
<%--This is the modal for creating a new Article--%>

<div class="modal fade" id="newArticleModal" tabindex="-1" role="dialog" aria-labelledby="modal_title"
     aria-hidden="true" style="width: 80%; height: 100%; margin-left: 10%">

    <div class="modal-content">
        <div class="modal-body">

            <%--The writer creates new content here, and can specify a date and time of publication if they so desire--%>
            <form method="post" action="Articles">
                <p>Please Enter a title</p>
                <input type="text" name="newTitle" onclick="" required>
                <span style="color: red">*</span>

                <p>Content:</p>
                <textarea class="summernote" name="newContent"></textarea>


                <p>Publish Date:</p>
                <input type="date" name="yearMD" value="">
                <input type="time" name="hourMS" id="myTime" value="">

                <input type="submit" onclick="makeNewArticle()">

            </form>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>
<%--chinchien: WYSIWYG--%>
<script>
    $('.summernote').summernote({
        height: 300,
        focus: true
    });
</script>