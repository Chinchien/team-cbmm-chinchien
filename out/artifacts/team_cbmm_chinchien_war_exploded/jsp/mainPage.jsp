<%@ page import="java.util.List" %>
<%@ page import="DAOs.Article" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="DAOs.User" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main Page</title>
    <%--summernote--%>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <%--my css--%>
    <link rel="stylesheet" type="text/css" href="css/mainPage.css"/>
    <link rel="stylesheet" type="text/css" href="css/website.css"/>
</head>

<body class="bodyStyle">
    <%@ include file="navbar.jsp" %>
<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <img src="image/image01.png" alt="Technology" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Technology</h3>
                    <p>Technology is connecting the World at a speed of 'speed of 'light' '</p>
                </div>
            </div>

            <div class="item">
                <a href="http://www.ictgraduateschool.ac.nz/" target="_blank"><img src="image/image04.png" alt="Health"
                                                                                   style="width:100%;"></a>
                <div class="carousel-caption">
                </div>
            </div>

            <div class="item">
                <img src="image/image03.png" alt="Education" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Education</h3>
                    <p>Learning new things 'FastPace' is like taking an daily dose of Adrenaline!!</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <div class="tab1">
        <%--<div class="col-md-12 row">--%>
        <div class="col-md-12 row getRidOfpad">
            <form class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                <div class="col-md-12">
                    <input class="col-md-9 searchArticle" name="command" type="text"
                           placeholder="Search article or author.." class="allInput" value="${command}"/>
                    <input class="col-md-2 col-md-offset-1 btn btn-dange searchButton" type="submit" class="tabSub"
                           value="Search"/>
                </div>
            </form>
            <form class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                <div class="col-md-12">
                    <input class="col-md-9 searchDate" type="date" name="date" value="${date}">
                    <input class="col-md-2 col-md-offset-1 btn  searchButton" type="submit">
                </div>
            </form>

            </div>
            <br>
            <div class="col-md-12 row getRidOfpad" style="margin-top: 10px">
                <form  class="col-md-4 col-md-offset-8 getRidOfpad" action="SearchServlet" method="post">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle dropBtn" type="button" data-toggle="dropdown">Sort Date By
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><input class="sortBtn" type="submit" name="sortForward" value="Descending"></li>
                                <li><input class="sortBtn" type="submit" name="sortBackward" value="Ascending"></li>
                            </ul>
                        </div>
                </form>
            </div>
    </div>
</div>


<div class="">
    <div class="container container-margin">
        <c:choose>
            <c:when test="${fn:length(articles) gt 0}">

                <c:forEach var="article" items="${articles}">
                    <div class="row contentCard02 col-md-12" id="${article.articleId}">
                        <div class="">
                            <div class="innerContent">
                                <h5 class="card-title"> ${article.title}</h5>
                                <p class="author">By / <span>${article.username}</span>
                                    <span>${article.modifiedDateAndTime}</span></p>
                                <div class="articleContent">
                                </div>

                                <button type="button" class="btn load btn-set col-md-6" data-toggle="modal"
                                        data-target="#articleModal"
                                        data-username="${article.username}" data-content="${article.content}"
                                        data-title="${article.title}"
                                        data-id="${article.articleId}"   onclick="displayNestedComments(${article.articleId}); showDelete()">
                                    <img src="image/read.png" alt="Technology" style="width:20px;">
                                    Load Article
                                </button>
                                    <%--Chinchien view blog--%>
                                <button class="btn load btn-set col-md-6"
                                        onclick="location.href='HomePage?status=${article.username}';"
                                        value="View Blog">
                                    <img src="image/read.png" alt="Technology" style="width:20px;">
                                    View Blog
                                </button>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${username == article.username}">
                                <button type="button" id="deleteButton"
                                        class="btn load col-md-1 btn-delete col-md-offset-11"
                                        onclick="deleteArticle(${article.articleId})"
                                        data-articleID="">
                                    <img src="image/trashbin.png" alt="Technology" style="width:20px;">
                                    delete
                                </button>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <p>No articles!</p>
            </c:otherwise>
        </c:choose>
        <br>
    </div>
</div>


<%@include file="articleModal.jsp" %>
<%@include file="nestedCommentModal.jsp" %>



<script>
    function leaveComment() {

        document.cookie = "edit=create";
        var comment = $('#articleModal #commentTextArea');
        var myComment = comment.val();
        var articleID = $('#articleModal #modal_id');
        var articleIDText = articleID.text();
        var commentArea = $("#articleModal #commentArea");

        //comment area is appended with a 'comment box' containing image of the person who left it, and delete and nested comment buttons.

        commentArea.append("<div class=\"commentz\" > <img src=\"image/${image}\" class=\"tinyImg\">  " + myComment + " <button data-toggle=\"modal\"" +
            "                                        data-target=\"#nestedModal\" class=\"smallClick\" >" +
            "<img class=\"cmtImgz\" src=\"image/comment.png\">" +

            "</button>" +

            " <button type=\"button\" class=\"ersatzDelete\" id=\"deleteButton\""+
            "> <img class=\"dltCmtImgz\" src=\"image/delete.png\">"+
            "</button>");

        $('#articleModal #commentTextArea').val(" ");
        var commentData = {text: myComment, articleID: articleIDText};
        $.ajax({
            type: 'post',
            url: "/Comment",
            data: commentData,
            dataType: "text",
            success: function (resultData) {
                alert("Save Complete")
            }
        });

    }



    function leaveNestedComment() {
        var referenceArea=$("#nestedModalz #referenceID");
        var referenceIDz=referenceArea.text();
        var comment = $('#nestedModalz #leaveComment');
        var myComment = comment.val();
        var articleID = $('#articleModal #modal_id');
        var articleIDText = articleID.text();


        $('#'+referenceIDz).append("<div class=\"tinyCommentsz\" > <img src=\"image/${image}\" class=\"tinyImg\">"+myComment+
            " <button type=\"button\" class=\"ersatzDelete\" id=\"deleteButton\""+
            "> <img class=\"dltCmtImgz\" src=\"image/delete.png\">"+
            "</button>"+
            "<button"+
        "onclick=\"personalSpace() \" class=\"smallClick\">"+
            "<img class=\"cmtImgz\" src=\"image/comment.png\">"+
            "</button>"+
            "</div>");


        document.cookie = "edit=create";

        var nestCommentData = {text: myComment, rID: referenceID, articleID: articleIDText};
        $.ajax({
            type: 'post',
            url: "/Comment",
            data: nestCommentData,
            dataType: "text",
            success: function (resultData) {


                alert("Save Complete")
            }
        });


        $('.commentingText').css("display", "none");

    }


</script>

<script src="js/articleFunctions.js"></script>
</body>
</html>
