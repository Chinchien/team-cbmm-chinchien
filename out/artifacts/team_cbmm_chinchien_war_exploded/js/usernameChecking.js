/**
 * created by: Chinchien
 * - use ajax to check if the username already exits in database
 * */
function checkUsername() {
    $.ajax({
        url: 'UsernameCheckingServlet',
        type: 'POST',
        data: {inputUsername: $("#userName").val()},
        dataType: "json",
        success: function (msg) {
            if (!msg.errorMsg == "") {
                alert(msg.errorMsg);
                $("#userName").val("");
                $("#userName").focus();
            }
        },
        error: function () {
            // no return msg = no match
        }
    });
}